package com.tnwt.maven.demo;

public class Device {

	private String name = "device";
	private double power = 100.00;
	private double duration = 1.00;

	public String getName() {
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}

	public double getPower() {
		return power;
	}
	
	public void setPower(double p)
	{
		if(p <= 0 || p > 10000) throw new RuntimeException();
		power = p;
	}

	public double getDuration() {
		return duration;
	}

	public void setDuration(double d) {
		if (d <= 0 || d > 24) throw new RuntimeException();
		this.duration = d;
	}
	
}
