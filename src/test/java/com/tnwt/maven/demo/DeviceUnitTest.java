package com.tnwt.maven.demo;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DeviceUnitTest {
	
	private Device d;
	@Before
	public void before() {
		d = new Device();
	}

	@Test
	public void default_name_should_be_device() {
		String name = d.getName();
		assertEquals("device", name);
	}
	
	@Test
	public void default_power_should_be_100() {
		double power = d.getPower();
		assertEquals(100.00, power, 0.01);
	}
	
	@Test
	public void default_duration_should_be_1() {
		double duration = d.getDuration();
		assertEquals(1.00, duration, 0.01);
	}
	
	@Test
	public void should_be_able_to_set_duration() {
		double expected = 4.0;
		d.setDuration(expected);
		double duration = d.getDuration();
		assertEquals(expected, duration, 0.01);
	}
	
	@Test(expected=RuntimeException.class)
	public void set_duration_to_0_should_throw_RuntimeException() {
		d.setDuration(0.0);
	}
	
	@Test(expected=RuntimeException.class)
	public void set_duration_to_less_than_0_should_throw_RuntimeException() {
		d.setDuration(-0.0001);
	}
	
	@Test
	public void should_be_able_to_set_duration_to_24() {
		double expected = 24.0;
		d.setDuration(expected);
		double duration = d.getDuration();
		assertEquals(expected, duration, 0.01);
	}
	
	@Test(expected=RuntimeException.class)
	public void set_duration_to_24_01_should_throw_RuntimeException() {
		d.setDuration(24.01);
	}
	
	@Test
	public void shold_able_to_set_power()
	{
		int expected = 10;
		d.setPower(expected);
		double power = d.getPower();
		assertEquals(expected, power,0.01);
	}
	
	@Test(expected=RuntimeException.class)
	public void set_power_to_0_should_throw_RuntimeException()
	{
		d.setPower(0);
	}
	
	@Test
	public void should_be_able_to_set_power_to_0_01()
	{
		double expected = 0.01;
		d.setPower(expected);
		double power = d.getPower();
		assertEquals(expected, power, 0.01);
	}
	
	@Test(expected=RuntimeException.class)
	public void set_power_to_negative_1_should_throw_runtimeexception()
	{
		d.setPower(-1);
	}
	
	@Test
	public void should_be_able_to_set_power_to_10000() {
		int expected = 10000;
		d.setPower(expected);
		double power = d.getPower();
		assertEquals(expected, power, 0.01);
	}
	
	@Test(expected=RuntimeException.class)
	public void set_power_to_10000_01_should_throw_RuntimeException() {
		d.setPower(10000.01);
	}
	
	@Test
	public void should_be_able_to_set_name_to_devicename()
	{
		String expected = "devicename";
		d.setName(expected);
		String dname = d.getName();
		assertEquals(expected, dname);
	}
}
